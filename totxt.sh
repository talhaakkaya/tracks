#!/bin/bash

while read -r track
do
	SERVER_NAME=$(echo $track | awk '{split($0,x,"|"); print x[1]}')
	TRACK_URI=$(echo $track | awk '{split($0,x,"|"); print x[2]}')
	PLAY_DATE=$(echo $track | awk '{split($0,x,"|"); print x[3]}')
	echo $TRACK_URI > $SERVER_NAME/$PLAY_DATE
done < <(cat $1) 
